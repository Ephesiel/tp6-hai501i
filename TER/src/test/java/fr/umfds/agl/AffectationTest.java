package fr.umfds.agl;

import fr.umfds.agl.table.GestionTable;
import fr.umfds.agl.table.Groupe;
import fr.umfds.agl.table.Sujet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AffectationTest {
    @Test
    void testAffectation() {
        GestionTable.genereTable();

        Sujet sujet = GestionTable.sujets.get(1);
        Groupe groupe = GestionTable.groupes.get(1);

        Affectation.affecteSujetToGroupe(groupe, sujet);

        Assertions.assertEquals(groupe.getId(), sujet.getGroupeAffecte().getId());
        Assertions.assertEquals(sujet.getId(), groupe.getSujet().getId());
    }
}