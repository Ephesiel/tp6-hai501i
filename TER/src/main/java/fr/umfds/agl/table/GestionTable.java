package fr.umfds.agl.table;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.nio.file.Paths;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class GestionTable {
    static ObjectMapper objectMapper = new ObjectMapper();

    public static HashMap<Integer, Etudiant> etudiants = new HashMap<>();
    public static HashMap<Integer, Enseignant> enseignants = new HashMap<>();
    public static HashMap<Integer, Groupe> groupes = new HashMap<>();
    public static HashMap<Integer, Sujet> sujets = new HashMap<>();
    public static HashMap<Integer, Voeu> voeux = new HashMap<>();

    static public void genereTable() {
        try {
            File etudiantsFile = Paths.get("src/main/resources/etudiants.json").toFile();
            File enseignantsFile = Paths.get("src/main/resources/enseignants.json").toFile();
            File groupesFile = Paths.get("src/main/resources/groupes.json").toFile();
            File sujetsFile = Paths.get("src/main/resources/sujets.json").toFile();
            File voeuxFile = Paths.get("src/main/resources/voeux.json").toFile();

            JsonNode etudiantsJson = objectMapper.readTree(etudiantsFile);
            JsonNode enseignantsJson = objectMapper.readTree(enseignantsFile);
            JsonNode groupesJson = objectMapper.readTree(groupesFile);
            JsonNode sujetsJson = objectMapper.readTree(sujetsFile);
            JsonNode voeuxJson = objectMapper.readTree(voeuxFile);

            for (JsonNode etudiantNode : etudiantsJson) {
                Etudiant etudiant = new Etudiant(etudiantNode);
                etudiants.put(etudiant.getId(), etudiant);
            }

            for (JsonNode enseignantNode : enseignantsJson) {
                Enseignant enseignant = new Enseignant(enseignantNode);
                enseignants.put(enseignant.getId(), enseignant);
            }

            for (JsonNode groupeNode : groupesJson) {
                Groupe groupe = new Groupe(groupeNode, etudiants);
                groupes.put(groupe.getId(), groupe);
            }

            for (JsonNode sujetNode : sujetsJson) {
                Sujet sujet = new Sujet(sujetNode, enseignants);
                sujets.put(sujet.getId(), sujet);
            }

            for (JsonNode voeuNode : voeuxJson) {
                Voeu voeu = new Voeu(voeuNode, sujets, groupes);
                voeux.put(voeu.getId(), voeu);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static public void serialize() throws IOException {
        List<ObjectNode> etudiantsObjects = new ArrayList<>();
        List<ObjectNode> enseignantsObjects = new ArrayList<>();
        List<ObjectNode> groupesObjects = new ArrayList<>();
        List<ObjectNode> sujetsObjects = new ArrayList<>();
        List<ObjectNode> voeuxObjects = new ArrayList<>();

        for (Etudiant etudiant : etudiants.values()) {
            etudiantsObjects.add(etudiant.serialize(objectMapper));
        }

        for (Enseignant enseignant : enseignants.values()) {
            enseignantsObjects.add(enseignant.serialize(objectMapper));
        }

        for (Groupe groupe : groupes.values()) {
            groupesObjects.add(groupe.serialize(objectMapper));
        }

        for (Sujet sujet : sujets.values()) {
            sujetsObjects.add(sujet.serialize(objectMapper));
        }

        for (Voeu voeu : voeux.values()) {
            voeuxObjects.add(voeu.serialize(objectMapper));
        }

        String etudiantsSerialized = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectMapper.valueToTree(etudiantsObjects));
        String enseignantsSerialized = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectMapper.valueToTree(enseignantsObjects));
        String groupesSerialized = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectMapper.valueToTree(groupesObjects));
        String sujetsSerialized = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectMapper.valueToTree(sujetsObjects));
        String voeuxSerialized = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectMapper.valueToTree(voeuxObjects));

        FileWriter etudiantsSerializedFile = new FileWriter("src/main/resources/etudiants-serialized.json");
        FileWriter enseignantsSerializedFile = new FileWriter("src/main/resources/enseignants-serialized.json");
        FileWriter groupesSerializedFile = new FileWriter("src/main/resources/groupes-serialized.json");
        FileWriter sujetsSerializedFile = new FileWriter("src/main/resources/sujets-serialized.json");
        FileWriter voeuxSerializedFile = new FileWriter("src/main/resources/voeux-serialized.json");

        etudiantsSerializedFile.write(etudiantsSerialized);
        enseignantsSerializedFile.write(enseignantsSerialized);
        groupesSerializedFile.write(groupesSerialized);
        sujetsSerializedFile.write(sujetsSerialized);
        voeuxSerializedFile.write(voeuxSerialized);

        etudiantsSerializedFile.close();
        enseignantsSerializedFile.close();
        groupesSerializedFile.close();
        sujetsSerializedFile.close();
        voeuxSerializedFile.close();
    }
}
