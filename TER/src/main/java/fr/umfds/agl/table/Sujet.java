package fr.umfds.agl.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.ArrayList;

public class Sujet {
    private int id;
    private String titre;
    private String resume;
    private Enseignant encadrant;
    private Groupe groupeAffecte;

    public Sujet(JsonNode datas, HashMap<Integer, Enseignant> enseignants) {
        this.id = datas.get("id").asInt();
        this.titre = datas.get("titre").asText();
        this.resume = datas.get("resume").asText();
        this.encadrant = enseignants.get(datas.get("encadrant").asInt());
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public void setEncadrant(Enseignant encadrant) {
        this.encadrant = encadrant;
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getResume() {
        return resume;
    }

    public Enseignant getEncadrant() {
        return encadrant;
    }

    public String toString() {
        return "[" + id + "] " + titre + "\nEncadrant : " + encadrant.toString() + "\nRésumé : " + resume;
    }

    public ObjectNode serialize(ObjectMapper mapper) {
        ObjectNode node = mapper.createObjectNode();

        node.put("id", id);
        node.put("titre", titre);
        node.put("resume", resume);
        node.put("encadrant", encadrant.getId());

        return node;
    }

    public Groupe getGroupeAffecte() {
        return groupeAffecte;
    }

    public void setGroupeAffecte(Groupe groupeAffecte) {
        this.groupeAffecte = groupeAffecte;
    }
}