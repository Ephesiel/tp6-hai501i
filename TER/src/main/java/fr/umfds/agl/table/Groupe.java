package fr.umfds.agl.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Groupe {
    private int id;
    private String nom;
    private List<Etudiant> etudiants = new ArrayList<>();
    private Sujet sujet = null;

    public Groupe(JsonNode datas, Map<Integer, Etudiant> etudiants) {
        this.id = datas.get("id").asInt();
        this.nom = datas.get("nom").asText();
        for (JsonNode node : datas.get("etudiants")) {
            this.etudiants.add(etudiants.get(((IntNode) node).numberValue()));
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEtudiants(List<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public List<Etudiant> getEtudiants() {
        return etudiants;
    }
    
    public String toString() {
        return "[" + id + "] " + nom + " - " + etudiants.toString();
    }

    public ObjectNode serialize(ObjectMapper mapper) {
        ObjectNode node = mapper.createObjectNode();
        
        List<Integer> ids = etudiants.stream().map(Etudiant::getId).collect(Collectors.toList());

        node.put("id", id);
        node.put("nom", nom);
        node.putArray("etudiants").addAll((ArrayNode) mapper.valueToTree(ids));

        return node;
    }

    public Sujet getSujet() {
        return sujet;
    }

    public void setSujet(Sujet sujet) {
        this.sujet = sujet;
    }
}