package fr.umfds.agl.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.ArrayList;

public class Voeu {
    private int ordre;
    private Sujet sujet;
    private Groupe groupe;

    public Voeu(JsonNode datas, HashMap<Integer, Sujet> sujets, HashMap<Integer, Groupe> groupes) {
        this.ordre = datas.get("ordre").asInt();
        this.sujet = sujets.get(datas.get("sujet").asInt());
        this.groupe = groupes.get(datas.get("groupe").asInt());
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    public void setSujet(Sujet sujet) {
        this.sujet = sujet;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public int getId() {
        return sujet.getId() * 10000 + groupe.getId();
    }

    public int getOrdre() {
        return ordre;
    }

    public Sujet getSujet() {
        return sujet;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public String toString() {
        return "[" + ordre + "] Sujet : " + sujet.toString() + ", groupe : " + groupe.toString(); 
    }

    public ObjectNode serialize(ObjectMapper mapper) {
        ObjectNode node = mapper.createObjectNode();

        node.put("ordre", ordre);
        node.put("sujet", sujet.getId());
        node.put("groupe", groupe.getId());

        return node;
    }
}