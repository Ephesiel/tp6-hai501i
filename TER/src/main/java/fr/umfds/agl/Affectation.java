package fr.umfds.agl;

import fr.umfds.agl.table.Groupe;
import fr.umfds.agl.table.Sujet;

public class Affectation {
    public static void affecteSujetToGroupe(Groupe groupe, Sujet sujet)  {
        sujet.setGroupeAffecte(groupe);
        groupe.setSujet(sujet);
    }
}
